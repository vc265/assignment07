//Vicky Chou

$(document).ready(function() {
    // hides footer
    $("footer > a").click(function() {
        $("footer").hide();
    })
    
    // animates car
    $("#car").click(function() {
        $("#car").animate({left: "625px"}, {duration: 1000});
    })
    
    // toggles ghost
    $("#container > p:last-child").click(function() {
        $("#ghost").toggle(3000);
    })
    
    // sets spring theme
    $("#setSpringTheme").click(function() {
        ($("body").css("background-color", "#FFDAB9"));
        ($("nav").css("background-color", "#FFA500"));
    })
    
    //sets fall theme
    $("#setFallTheme").click(function() {
        ($("body").css("background-color", "#D8ECC3"));
        ($("nav").css("background-color", "#1C4905"));
    })
})